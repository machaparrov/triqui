import random

class jugador:
    
    def realizarJugada(self):
        
        #TO DO ¿De donde saco la matriz?
        simbolo=self.seleccionaJugador()
        
        if simbolo=='X':
            self.jugarComputador()
        else:
            self.jugarUsuario()
            
            
    
    
    
    def seleccionaJugador(self,simbolo):
        if simbolo=='X':
            simbolo='O'
        else:
            simbolo='X'
        return simbolo
    
    
    def jugarUsuario(self, tablero, simbolo):
        
        terminar=False
        
        while not terminar:
            
            #pedir dos numeros al usuario
            x=int(input("ingresar x:"))
            y=int(input("ingresar y:"))
            
            #verificar si la casilla del tablero esta vacia
            
            if not (tablero[x][y]=='O' or tablero[x][y]=='X'):
                terminar=True
                
        tablero[x][y]=simbolo 
        
        return tablero 
    
    def jugarComputador(self, tablero, simbolo):
        
        terminar=False
        
        while not terminar:
            
            #pedir dos numeros al usuario
           
            x=random.randint(0,2)
            y=random.randint(0,2)
            #verificar si la casilla del tablero esta vacia
            
            if not (tablero[x][y]=='O' or tablero[x][y]=='X'):
                
                terminar=True
                
        tablero[x][y]=simbolo 
        
        return tablero 